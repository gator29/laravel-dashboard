{{-- TODO: place modal in center and darken the page --}}
<div class="fixed inset-0 flex items-center justify-center z-[9999]">
    <div class="absolute bg-black opacity-50 w-full h-full"></div>
    <div tabindex="-1" aria-hidden="true" class="rounded-lg shadow-lg z-[99999] overflow-y-auto max-w-2xl max-h-full">
    {{-- <div class="absolute top-0 left-0 w-full h-full bg-black bg-opacity-50 "></div>
        <div class="fixed left-0 top-0  h-full w-full overflow-y-auto overflow-x-hidden outline-none"> --}}
            <div class="relative w-full ">
                <!-- Modal content -->
                <div class="relative bg-white rounded-lg shadow dark:bg-gray-700">
                    <!-- Modal header -->
                    <div class="flex items-start justify-between p-4 border-b rounded-t dark:border-gray-600">
                        <h3 class="text-xl font-semibold text-gray-900 dark:text-white">
                            Add task
                        </h3>
                        <div class="cursor-pointer" wire:click="$emit('closeTodoModal')">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6 text-gray-500 hover:text-gray-700 dark:text-gray-400 dark:hover:text-gray-200">
                                <path stroke-linecap="round" stroke-linejoin="round" d="M9.75 9.75l4.5 4.5m0-4.5l-4.5 4.5M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
                            </svg>
                        </div>
                    </div>
                    <!-- Modal body -->
                    <div  class="p-6 space-y-6">
                        <!-- Your form goes here -->
                        <form wire:submit.prevent="store">
                            <div class="shadow overflow-hidden sm:rounded-md">
                                <div class="px-4 py-5 bg-white sm:p-6">
                                    <div class="grid grid-cols-6 gap-6">

                                        <div class="col-span-6 sm:col-span-4">
                                            <label for="title" class="block text-sm font-medium text-gray-700">Title</label>
                                            <input type="text" name="title" id="title" wire:model="title" class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                                            @error('title') <span>{{ $message }}</span> @enderror
                                        </div>

                                        <div class="col-span-6 sm:col-span-3">
                                            <label for="desc" class="block text-sm font-medium text-gray-700">Description</label>
                                            <input type="text" name="desc" id="desc" wire:model="description" class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                                            @error('description') <span>{{ $message }}</span> @enderror
                                        </div>

                                        <div class="col-span-6 sm:col-span-3">
                                            <label for="activity" class="block text-sm font-medium text-gray-700">Activity</label>
                                            <input type="text" name="activity" id="activity" wire:model="activity" class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                                            @error('activity') <span>{{ $message }}</span> @enderror
                                        </div>

                                        <div class="col-span-6 sm:col-span-3">
                                            {{-- TODO: make a select input type for priority with icons --}}
                                            <label for="priority" class="block text-sm font-medium text-gray-700">Priority</label>
                                            <input type="text" name="priority" id="priority"  wire:model="priority" class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                                            @error('priority') <span>{{ $message }}</span> @enderror
                                        </div>

                                    </div>
                                </div>
                                <div class="px-4 py-3 bg-gray-50 text-right sm:px-6">
                                    <button type="submit" class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                                        Save
                                    </button>
                                </div>
                            </div>
                            @csrf
                        </form>
                    </div>
                </div>
            </div>
        </div>

</div>
