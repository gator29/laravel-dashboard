<div class="text-gray-600 dark:text-gray-400 col-span-6 row-span-1 mb-2 border-solid dark:border-[#3c4043] rounded border shadow-sm">
    <div class="bg-grey-lighter px-2 py-3 border-solid dark:border-[#3c4043] border-b flex justify-between items-center">
        <div>
            Todo-list
        </div>
        <div class="cursor-pointer" wire:click="$emit('openTodoModal')">
            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6 text-gray-500 hover:text-gray-700 dark:text-gray-400 dark:hover:text-gray-200">
                <path stroke-linecap="round" stroke-linejoin="round" d="M12 9v6m3-3H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z" />
            </svg>
        </div>
        @if ($showTodoModal)
            @livewire('todo.add')
        @endif
    </div>
    <div class="p-3">
        <div class="flex flex-col">
            <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
            <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                <table class="min-w-full divide-y divide-gray-200 table-auto" id="TodoListTable">
                    <thead class="bg-gray-50">
                    <tr>
                        <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                        Title
                        </th>
                        <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                        Description
                        </th>
                        <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                        Activity
                        </th>
                        <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                        Priority
                        </th>
                        <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                        </th>
                    </tr>
                    </thead>
                    <tbody class="bg-white divide-y divide-gray-200">
                    @forelse ( $tasks as $r )
                        <tr>
                            <td class="px-6 py-4 whitespace-nowrap">
                                <div class="flex items-center">
                                    {{-- <div class="flex-shrink-0 h-10 w-10">
                                    <img class="h-10 w-10 rounded-full" src="https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=4&w=256&h=256&q=60" alt="">
                                    </div> --}}
                                    <div class="ml-4">
                                        <div class="text-sm font-medium text-gray-900" data-id="{{ $r->id }}">
                                            {{ $r->title }}
                                            {{-- hardcoded dummy text --}}
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <td class="px-6 py-4 whitespace-nowrap">
                                <div class="text-sm text-gray-900" data-id="{{ $r->id }}">
                                    {{ $r->description }}
                                    {{-- hardcoded dummy text --}}
                                </div>
                            </td>
                            <td class="px-6 py-4 whitespace-nowrap">
                                <div class="text-sm font-medium text-gray-900" data-id="{{ $r->id }}">
                                    {{ $r->activity }}
                                    {{-- hardcoded dummy text --}}
                                </div>
                            </td>
                            <td class="px-6 py-4 whitespace-nowrap">
                                <div class="text-sm font-medium text-gray-900" data-id="{{ $r->id }}">
                                    {{ $r->priority }}
                                    {{-- hardcoded dummy text --}}
                                </div>
                            </td>
                            <td class="px-6 py-4 whitespace-nowrap">
                                {{-- delete icon --}}
                                {{-- TODO: create animation on delete --}}
                                    <meta name="csrf-token" content="{{ csrf_token() }}">
                                    <button class="bg-red-700 rounded deleteTodoTask" data-id="{{ $r->id }}">
                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="white" class="w-6 h-6">
                                            <path stroke-linecap="round" stroke-linejoin="round" d="M14.74 9l-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 01-2.244 2.077H8.084a2.25 2.25 0 01-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 00-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 013.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 00-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 00-7.5 0" />
                                        </svg>
                                    </button>
                                {{-- /delete icon --}}
                            </td>
                        </tr>
                    @empty
                        {{-- TODO: remove this text when task has been added with broadcast and show when all tasks has been deleted --}}
                        No tasks todo!
                    @endforelse
                    </tbody>
                </table>
                </div>
            </div>
            </div>
        </div>
        @push('scripts')
            <script type="module">
                window.onload = function() {
                    Echo.channel(`TodoList`)
                        .listen('.TodoListCreated', (todo) => {
                            console.log(todo);
                            $('#TodoListTable tbody').append(
                                `<tr>
                                    <td class="px-6 py-4 whitespace-nowrap">
                                        <div class="flex items-center">
                                            <div class="ml-4">
                                                <div class="text-sm font-medium text-gray-900" data-id="`+todo.model.id+`">
                                                    `+todo.model.title+`
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="px-6 py-4 whitespace-nowrap">
                                        <div class="text-sm text-gray-900" data-id="`+todo.model.id+`">
                                            `+todo.model.description+`
                                        </div>
                                    </td>
                                    <td class="px-6 py-4 whitespace-nowrap">
                                        <div class="text-sm font-medium text-gray-900" data-id="`+todo.model.id+`">
                                            `+todo.model.activity+`
                                        </div>
                                    </td>
                                    <td class="px-6 py-4 whitespace-nowrap">
                                        <div class="text-sm font-medium text-gray-900" data-id="`+todo.model.id+`">
                                            `+todo.model.priority+`
                                        </div>
                                    </td>
                                    <td class="px-6 py-4 whitespace-nowrap">
                                        {{-- delete icon --}}
                                        <meta name="csrf-token" content="{{ csrf_token() }}">
                                        <button class="bg-red-700 rounded deleteTodoTask" data-id="`+todo.model.id+`">
                                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="white" class="w-6 h-6">
                                                <path stroke-linecap="round" stroke-linejoin="round" d="M14.74 9l-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 01-2.244 2.077H8.084a2.25 2.25 0 01-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 00-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 013.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 00-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 00-7.5 0" />
                                            </svg>
                                        </button>
                                        {{-- /delete icon --}}
                                    </td>
                                </tr>`
                            );
                        })
                        .listen('.TodoListDeleted', (todo) => {
                            console.log(todo);
                            $('div[data-id="'+todo.model.id+'"]').parent().parent().remove();
                        });
                    }

            </script>
            <script>
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $(document).on("click", ".deleteTodoTask", function(e){
                    e.preventDefault();
                    var id = $(this).data("id");
                    var token = $("meta[name='csrf-token']").attr("content");
                    $.ajax(
                    {
                        url: "/todo/delete/"+id,
                        type: 'DELETE',
                        data: {
                            "id": id,
                            "_token": token,
                        },
                        success: function (response){
                            console.log(response);
                            $('div[data-id="'+id+'"]').parent().parent().remove();
                        }
                    });
                });
                // OLD CRONJOB CODE
                var job = new CronJob(
                    '0 11 15 * * *',
                    function() {
                        $.ajax({
                            type: 'get',
                            url: '/todo/cronSearch',
                            success: function(data){
                                var result = 0;
                                console.log(data);
                                /*
                                * FROM THIS POINT, PUT THE DATA IN THE IF STATEMENT IN THE SAME STYLE AS THE ABOVE HTML CODE
                                * */

                                // if(data.length != 0) {
                                //     $.each(data, function()	{
                                //         // IF NO ID, DO SOMETHING
                                //         if(data[result].hasOwnProperty('id')) {
                                //             $('.search-results').append('<li><a href="/randomizer/show/'+data[result].id+'">'+ data[result].title +'</a></li>');
                                //         } else if(!data[result].hasOwnProperty('id')) {
                                //             $('.search-results').append('<p>'+ data[result].title +'</p>');
                                //         }
                                //         result++;
                                //     });
                                // } else {
                                //     $("#WorkTimeTable tbody").empty();
                                //     $("#WorkTimeTable tbody").append("<p>No worktime data found for today!</p>");
                                // }
                            }
                        });
                    }, null, true, 'Europe/Amsterdam'
                );
                // job.start();
            </script>
        @endpush
    </div>
</div>
