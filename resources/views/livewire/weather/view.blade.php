<div class="text-gray-600 dark:text-gray-400 col-span-3 row-span-1 mb-2 border-solid dark:border-[#3c4043] rounded border shadow-sm">
    <div class="bg-grey-lighter px-2 py-3 border-solid dark:border-[#3c4043] border-b flex justify-between items-center">
        <div>
            Weather
        </div>
        <div class="cursor-pointer" wire:click="$emit('refreshWeatherData')">
            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6 text-gray-500 hover:text-gray-700 dark:text-gray-400 dark:hover:text-gray-200">
                <path stroke-linecap="round" stroke-linejoin="round" d="M16.023 9.348h4.992v-.001M2.985 19.644v-4.992m0 0h4.992m-4.993 0l3.181 3.183a8.25 8.25 0 0013.803-3.7M4.031 9.865a8.25 8.25 0 0113.803-3.7l3.181 3.182m0-4.991v4.99" />
            </svg>
        </div>
    </div>
    <div class="p-3">
        @if(isset($weatherData['error']))
            {{ $weatherData['error'] }}
        @else
            Weather description: {{ $weatherData['weather_description'] }}
            <br>
            Temperature: {{ $weatherData['temperature'] }}°C
            <br>
            Feels like: {{ $weatherData['feels_like'] }}°C
            <br>
            Rain volume: {{ $weatherData['rain_volume'] }}mm
            <br>
            Wind strength: {{ $weatherData['wind_strength'] }}
            <br>
            Wind speed: {{ $weatherData['wind_speed'] }}m/s
            <br>
            Direction: {{ $weatherData['wind_direction'] }}
            <br>
            Humidity: {{ $weatherData['humidity'] }}%
            <br>
            Cloudiness: {{ $weatherData['clouds'] }}%
            <br>
            Sunrise time: {{ $weatherData['sunrise'] }}
            <br>
            Sunset time: {{ $weatherData['sunset'] }}
            <br>
            <img src="https://openweathermap.org/img/wn/{{ $weatherData['weather_icon'] }}@2x.png" alt="weather icon">
            <br>
            Data refresh: {{ $weatherData['dt']}}
        @endif
    </div>
    <script>
        var refreshWeather = new CronJob('0 * * * *', function() {
            Livewire.emit('refreshWeatherData');
            console.log('refreshWeather');
        }, null, true, 'Europe/Amsterdam');
        refreshWeather.start();
    </script>
</div>
