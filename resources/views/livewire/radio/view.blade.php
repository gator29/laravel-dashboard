<div class="text-gray-600 dark:text-gray-400 col-span-6 row-span-1 mb-2 border-solid dark:border-[#3c4043] rounded border shadow-sm">
    <div class="bg-grey-lighter px-2 py-3 border-solid dark:border-[#3c4043] border-b">
        Radio
    </div>
    <div class="w-full h-full justify-center items-center flex">
        <audio src="" id="hidden-player"></audio>
        <div id="player">
            <img src="" class="coverr" alt="" height="200" width="200" />
            <div class="player-song">
                <div class="title"></div>
                <div class="artist"></div>
                <progress value="0" max="1"></progress>
                <div class="timestamps">
                    <div class="time-now">0:00:00</div>
                </div>
                <div class="actions">
                    <div class="prev">
                        <i class="material-icons">fast_rewind</i>
                    </div>
                    <div class="play">
                        <i class="material-icons play-button">play_arrow</i>
                    </div>
                    <div class="next">
                        <i class="material-icons">fast_forward</i>
                    </div>
                    <div class="volume">
                        <div class="volumeControl">
                            <div class="volumeTrack">
                                <div class="volumeBar" style="height:{{ $settings->volume }}%;"></div>
                            </div>
                        </div>
                        <i class="material-icons" id="VolumeIcon">volume_up</i>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@push("scripts")
    <script>
        // TODO: Fix on click issue with echo and ajax
        // TODO: Clean up code and put it in radio.js
        var audio = $("#hidden-player");
        var icon = $("#VolumeIcon");
        var PrevVolume = $(".volumeBar").height();

        icon.on("click", function() {
            if (audio[0].volume == 0) {
                audio[0].volume = PrevVolume / 100;
                icon.text("volume_up");
                $(".volumeBar").css("height", PrevVolume + "%");
                console.log("click");
                volumeChange();

            } else {
                PrevVolume = $(".volumeBar").height();
                audio[0].volume = 0;
                icon.text("volume_off");
                $(".volumeBar").css("height", "0%");
                console.log("click");
                volumeChange();
            }
        });

        // on hold click and slide on volumeBar change height and volume
        $(".volumeControl").on("mousedown", function(e) {
            e.preventDefault();
            PrevVolume = $(".volumeBar").height();
            $(".volume").addClass("active");

            $(".volumeControl").mousemove(function(e) {
                var volumeBar = $(".volumeBar");
                var volumeTrack = volumeBar.parent();
                var volumeControl = volumeTrack.parent();
                var volume = 1 - (e.pageY - volumeTrack.offset().top) / volumeTrack.height();
                volume = Math.max(0, Math.min(1, volume));
                volumeBar.css("height", volume * 100 + "%");
                audio[0].volume = volume;
                if (volume == 0) {
                    icon.text("volume_off");
                } else if (volume < 0.5) {
                    icon.text("volume_down");
                } else {
                    icon.text("volume_up");
                }
            });
        });
        //stop moving the volume bar when mouse is released
        $(document).mouseup(function() {
            $(".volumeControl").off("mousemove");
            $(".volume").removeClass("active");
            if (Math.round(PrevVolume) != Math.round(audio[0].volume * 100)) {
                console.log("mouse up");
                volumeChange();
            }
        });

        function volumeChange() {
            $.ajax({
                url: "/api/radio/changeVolume",
                type: "POST",
                data: {
                    "volume": Math.round(audio[0].volume * 100)
                }
            });
        }
    </script>
    <script>
        const stations = RadioBrowser.searchStations({
            name: 'radio 10',
            countryCode: 'NL',
            limit: 5
        });
        stations.then((stationList) => {
            console.log(stationList[1]);
            $("#hidden-player").attr("src", stationList[1]["urlResolved"]);
            if ({{ $settings->paused }} == true) {
                $("#hidden-player").attr("paused", "true");
                $(".play-button").text("play_arrow");
            } else {
                $("#hidden-player").attr("autoplay", "true");
                $(".play-button").text("pause");
            }
            $("#hidden-player").prop("volume", {{ $settings->volume / 100 }});
            if (audio[0].volume == 0) {
                icon.text("volume_off");
            } else if (audio[0].volume < 0.5) {
                icon.text("volume_down");
            } else {
                icon.text("volume_up");
            }
        });
    </script>
    <script>
        Echo.channel("Radio")
            .listen(".RadioToggle", (toggle) => {
                console.log(toggle);
                console.log("echo: " + toggle.pause);
                if (toggle.pause == false) {
                    console.log("trigger play");
                    $("#hidden-player").trigger("play");
                    $(".play-button").text("pause");
                } else {
                    console.log("trigger pause");
                    $("#hidden-player").trigger("pause");
                    $(".play-button").text("play_arrow");
                }
            })
            .listen(".Volume", (volume) => {
                console.log(volume);
                console.log("echo: " + volume.volume);
                $("#hidden-player").prop("volume", volume.volume / 100);
                $(".volumeBar").css("height", volume.volume + "%");
                if (volume.volume == 0) {
                    icon.text("volume_off");
                } else if (volume.volume < 50) {
                    icon.text("volume_down");
                } else {
                    icon.text("volume_up");
                }
            });
    </script>
    <script src="/js/radio.js" defer></script>
@endpush
