<div class="text-gray-600 dark:text-gray-400 col-span-3 row-span-1 mb-2 border-solid dark:border-[#3c4043] rounded border shadow-sm">
    <div class="bg-grey-lighter px-2 py-3 border-solid dark:border-[#3c4043] border-b flex justify-between items-center">
        <div>
            Garbage
        </div>
        <div class="cursor-pointer" wire:click="$emit('refreshGarbageData')">
            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6 text-gray-500 hover:text-gray-700 dark:text-gray-400 dark:hover:text-gray-200">
                <path stroke-linecap="round" stroke-linejoin="round" d="M16.023 9.348h4.992v-.001M2.985 19.644v-4.992m0 0h4.992m-4.993 0l3.181 3.183a8.25 8.25 0 0013.803-3.7M4.031 9.865a8.25 8.25 0 0113.803-3.7l3.181 3.182m0-4.991v4.99" />
            </svg>
        </div>
    </div>
    <div class="p-3">
        @if(isset($garbageData['error']))
            {{ $garbageData['error'] }}
        @else
            <table class="border-collapse border border-slate-500">
                <thead>
                    <tr>
                        <th class="border border-slate-600">Type</th>
                        <th class="border border-slate-600">Date</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($garbageData as $garbage)
                        @switch($garbage['garbageTypeCode'])
                            @case("GFT")
                                <tr class="bg-lime-700">
                            @break
                            @case("PAPIER")
                                <tr class="bg-blue-700">
                            @break
                            @case("PMD")
                                <tr class="bg-gray-700">
                            @break
                            @default
                                <tr>
                        @endswitch
                            <td class="border border-slate-700">{{ $garbage['garbageType'] }}</td>
                            <td class="border border-slate-700">{{ date('d-m-Y', strtotime($garbage['date'])) }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        @endif
    </div>
</div>
