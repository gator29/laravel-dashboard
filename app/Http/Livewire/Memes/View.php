<?php

namespace App\Http\Livewire\Memes;

use Livewire\Component;
use Illuminate\Support\Facades\Http;

class View extends Component
{

    protected $listeners = ['getMeme'];

    public function render()
    {
        return view('livewire.memes.view', [
            'meme' => $this->getMeme()
        ]);
    }

    // TODO: find a way to get the image to fit the tile without destroying the layout or get a different api that returns an image of a fixed size
    // FIXME: figure out a way to ignore this component if the api key is not set or invalid and show a message to the user
    public function getMeme() {
        $api_key = config('custom.meme_key');

        try {
            $response = Http::withHeaders([
                'X-RapidAPI-Key' => $api_key,
                'X-RapidAPI-Host' => 'programming-memes-images.p.rapidapi.com'
            ])->get('https://programming-memes-images.p.rapidapi.com/v1/memes');
            return $response->json()[0];
        } catch (\Exception $e) {
            return ['error' => 'Could not get meme data, please reload the block or check your api key'];
        }



    }


}
