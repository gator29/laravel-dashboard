<?php

namespace App\Http\Livewire\Garbage;

use Livewire\Component;

class View extends Component
{

    protected $listeners = ['refreshGarbageData' => 'getGarbageData'];

    public function render()
    {
        return view('livewire.garbage.view', [
            'garbageData' => $this->getGarbageData(),
        ]);
    }

    // get data from external api
    public function getGarbageData()
    {
        $client = new \GuzzleHttp\Client();
        try {
        $response = $client->request('GET', 'https://www.rova.nl/api/waste-calendar/upcoming?postalcode=8016LD&houseNumber=36&addition=&take=5');
        } catch (\Exception $e) {
            return ['error' => 'Could not get garbage data, please reload the block or check your api key'];
        }
        if($response !== null) {
            $data = json_decode($response->getBody()->getContents(), true);
        } else {
            return ['error' => 'Could not get garbage data, please reload the block or check your api key'];
        }

        return $data;
    }

}
