<?php

namespace App\Http\Livewire\Clock;

use Livewire\Component;

class View extends Component
{
    public function render()
    {
        return view('livewire.clock.view');
    }
}
