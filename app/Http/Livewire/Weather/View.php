<?php

namespace App\Http\Livewire\Weather;

use Livewire\Component;

class View extends Component
{

    // public $weatherData;
    protected $listeners = ['refreshWeatherData' => 'getWeatherData'];

    public function render()
    {
        return view('livewire.weather.view', [
            'weatherData' => $this->getWeatherData(),
        ]);
    }

    // FIXME: figure out a way to ignore this module if the api key is not set or invalid and show a message to the user
    // Get the current weather data from openweathermap with weather icons, temperature and wind direction and strength in zwolle overijssel
    public function getWeatherData()
    {

        $api_key = config('custom.openweather_key');
        try {
            $weatherData = json_decode(file_get_contents('https://api.openweathermap.org/data/2.5/weather?lat=52.5167738&lon=6.083022&appid='.$api_key.'&units=metric'), true);
        } catch (\Exception $e) {
            return ['error' => 'Could not get weather data, please reload the block or check your api key'];
        }

        if (isset($weatherData['weather'])) {
            $weatherData['weather_icon'] = $weatherData['weather'][0]['icon'];
            $weatherData['weather_description'] = $weatherData['weather'][0]['description'];
            $weatherData['rain_volume'] = $weatherData['rain']['1h'] ?? 0;
            $weatherData['wind_direction'] = $this->getWindDirection($weatherData['wind']['deg']);
            $weatherData['wind_strength'] = $this->getWindStrength($weatherData['wind']['speed']);
            $weatherData['wind_speed'] = $weatherData['wind']['speed'];
            $weatherData['temperature'] = round($weatherData['main']['temp']);
            $weatherData['feels_like'] = round($weatherData['main']['feels_like']);
            $weatherData['humidity'] = $weatherData['main']['humidity'];
            $weatherData['clouds'] = $weatherData['clouds']['all'];
            $weatherData['sunrise'] = date("H:i:s", $weatherData['sys']['sunrise']);
            $weatherData['sunset'] = date("H:i:s", $weatherData['sys']['sunset']);
            $weatherData['dt'] = date("H:i:s", $weatherData['dt']);
        } else {
            return ['error' => 'Could not get weather data, please reload the block or check your api key'];
        }



        return $weatherData;
    }

    // convert wind direction in degrees to a wind direction
    public function getWindDirection($windDirection)
    {
        $windDirections = [
            'N' => [348.75, 11.25],
            'NNO' => [11.25, 33.75],
            'NO' => [33.75, 56.25],
            'ONO' => [56.25, 78.75],
            'O' => [78.75, 101.25],
            'OZO' => [101.25, 123.75],
            'ZO' => [123.75, 146.25],
            'ZZO' => [146.25, 168.75],
            'Z' => [168.75, 191.25],
            'ZZW' => [191.25, 213.75],
            'ZW' => [213.75, 236.25],
            'WZW' => [236.25, 258.75],
            'W' => [258.75, 281.25],
            'WNW' => [281.25, 303.75],
            'NW' => [303.75, 326.25],
            'NNW' => [326.25, 348.75],
        ];

        foreach ($windDirections as $key => $windDirectionRange) {
            if ($windDirection >= $windDirectionRange[0] && $windDirection < $windDirectionRange[1]) {
                return $key;
            }
        }
    }

    // convert wind speed in m/s to a wind strength
    public function getWindStrength($windSpeed)
    {
        $windSpeeds = [
            'Calm' => [0, 0.2],
            'Light air' => [0.3, 1.5],
            'Light breeze' => [1.6, 3.3],
            'Gentle breeze' => [3.4, 5.4],
            'Moderate breeze' => [5.5, 7.9],
            'Fresh breeze' => [8, 10.7],
            'Strong breeze' => [10.8, 13.8],
            'Near gale' => [13.9, 17.1],
            'Gale' => [17.2, 20.7],
            'Strong gale' => [20.8, 24.4],
            'Storm' => [24.5, 28.4],
            'Violent storm' => [28.5, 32.6],
            'Hurricane' => [32.7, 100],
        ];

        foreach ($windSpeeds as $key => $windSpeedRange) {
            if ($windSpeed >= $windSpeedRange[0] && $windSpeed < $windSpeedRange[1]) {
                return $key;
            }
        }
    }
}
