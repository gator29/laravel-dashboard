<?php
// TODO: make it so that only I can add tasks
namespace App\Http\Livewire\Todo;

use Livewire\Component;
use App\Models\TodoList;

class Add extends Component
{

    protected $listeners = ['closeTodoModal'];
    public $title;
    public $description;
    public $activity;
    public $priority;

    public function store()
    {
        // validate the fields with the requirements
        $this->validate([
            'title' => 'required|string|max:50',
            'description' => 'required|string|max:255',
            'activity' => 'nullable|string|max:50',
            'priority' => 'required|string|max:50',
        ]);

        $todoList = new TodoList();

        $todoList->title = $this->title;
        $todoList->description = $this->description;
        $todoList->activity = $this->activity;
        $todoList->priority = $this->priority;

        $todoList->save();

        $this->emit('closeTodoModal'); // Close the modal after the data has been saved

        $this->reset();
    }

    public function render()
    {
        return view('livewire.todo.add');
    }

    public function closeTodoModal() {
        //$this->emit('refreshData'); // Emit an event to refresh data in the parent component
        $this->emit('closeTodoModal');
    }
}
