<?php

namespace App\Http\Livewire\Todo;

use Livewire\Component;
use App\Models\TodoList;

class View extends Component
{

    protected $listeners = ['openTodoModal', 'closeTodoModal'];

    public $showTodoModal = false;

    public function render()
    {
        return view('livewire.todo.view', [
            "tasks" => $this->getTasks()
        ]);
    }

    public function getTasks() {
        $tasks = TodoList::all();
        return $tasks;
    }

    public function openTodoModal() {
        $this->showTodoModal = true;
    }

    public function closeTodoModal() {
        $this->showTodoModal = false;
    }

}
