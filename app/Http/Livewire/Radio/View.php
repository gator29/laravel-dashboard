<?php

namespace App\Http\Livewire\Radio;
use Illuminate\Support\Facades\Storage;

use Livewire\Component;

class View extends Component
{
    private $settings;

    public function __construct($settings)
    {
        $this->settings = json_decode(Storage::disk('public')->get('radioSettings.json'), false);
    }

    public function render()
    {
        return view('livewire.radio.view', [
            'settings' => $this->settings,
        ]);
    }
}
