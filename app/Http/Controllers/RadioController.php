<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Events\RadioEvent;
use App\Events\Radio\VolumeControl;

class RadioController extends Controller
{
    public function pause(Request $request) {
        broadcast(new RadioEvent($request->input('pause')))->toOthers();
        return response()->json(["pause" => true]);
    }

    public function createSettings() {
        // $settings = [
        //     "paused" => "false",
        //     "volume" => 100,
        // ];

        // Storage::disk('public')->put('radioSettings.json', json_encode($settings));

        echo asset('storage/radioSettings.json');
    }

    public function togglePaused(Request $request) {
        $pause = json_decode($request->input('pause'));
        broadcast(new RadioEvent($pause));
        $settings = json_decode(Storage::disk('public')->get('radioSettings.json'), true);
        $settings["paused"] = $request->input('pause');
        Storage::disk('public')->put('radioSettings.json', json_encode($settings));
        return response()->json(["paused" => $settings["paused"]]);
    }

    public function changeVolume(Request $request) {
        $volume = json_decode($request->input('volume'));
        broadcast(new VolumeControl($volume));
        $settings = json_decode(Storage::disk('public')->get('radioSettings.json'), true);
        $settings["volume"] = $request->input('volume');
        Storage::disk('public')->put('radioSettings.json', json_encode($settings));
        return response()->json(["volume" => $settings["volume"]]);
    }

}
