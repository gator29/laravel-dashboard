<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class DashboardController extends Controller
{
    public function index() {
        $settings = json_decode(Storage::disk('public')->get('radioSettings.json'), false);
        return view("welcome")->with("settings", $settings);
    }

    public function test() {
        return view("test");
    }
}
