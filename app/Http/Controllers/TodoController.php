<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TodoList;
use App\Models\User;
use Illuminate\Support\Facades\Auth;


class TodoController extends Controller
{

    public function deleteTask($id) {
        $todoList = TodoList::find($id);
        $todoList->delete();

        return response()->json(["success" => true]);
    }

    public function cronSearch(Request $req) {
        //TODO: refactor this to work with the new todolist table if this code is still needed
        // if($req->ajax()) {
        //     $data = WorkTime::Join("users", "work_times.user_id", "=", "users.id")
        //         ->where("date", "=", date("Y-m-d"))
        //         ->select("work_times.*", "users.name", "users.profile_photo_path")
        //         ->orderBy("users.name", "asc")
        //         ->get();

        //     return response()->json($data);
        // }
        return true;
    }
}
