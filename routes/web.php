<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get("/", "DashboardController@index");
Route::get("test", "DashboardController@test");
Route::get("todo/create", "TodoController@addTask");
Route::get("todo/cronSearch", "TodoController@cronSearch");
Route::post("todo/store", "TodoController@storeTask");
Route::delete("todo/delete/{id}", "TodoController@deleteTask");

Route::get("radio/settings", "RadioController@createSettings");

Route::middleware(["auth:sanctum", "verified"])->get("/dashboard", function () {
    return view("dashboard");
})->name("dashboard");
