<?php

return [
    'meme_key' => env('MEME_KEY'),
    'openweather_key' => env('OPENWEATHER_API_KEY'),
];
